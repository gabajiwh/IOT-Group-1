// RECIEVER ESP32 CODE

#include <esp_now.h>
#include <WiFi.h>

////////////////////////////////////////////////
// Local Variables                            //
////////////////////////////////////////////////
typedef struct struct_message {
  float AccelerationX;
  float AccelerationY;
  float AccelerationZ;
} struct_message;

struct_message myData;
////////////////////////////////////////////////

////////////////////////////////////////////////
// Procedure:   MOTOR VARIABLES                //
////////////////////////////////////////////////

int motor1Pin1 = 27; 
int motor1Pin2 = 26; 
int enable1Pin = 14; 

int motor2Pin1 = 19;
int motor2Pin2 = 5; 
int enable2Pin = 18; 

const int freq = 30000;
const int pwmChannel = 0;
const int resolution = 8;
int dutyCycle = 200;

int trigPin = 16;    // TRIG pin
int echoPin = 4;    // ECHO pin
float duration_us, distance_cm;
////////////////////////////////////////////////


////////////////////////////////////////////////
// Procedure: Recieve data from the sender    //
////////////////////////////////////////////////
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  // Serial.print("Data received: ");
  // Serial.println(len);
  // Serial.print("Acc X: ");
  // Serial.println(myData.AccelerationX);
  // Serial.print("Acc Y: ");
  // Serial.println(myData.AccelerationY);
  // Serial.print("Acc Z: ");
  // Serial.println(myData.AccelerationZ);
  // Serial.println();
}
////////////////////////////////////////////////


void setup() {
  ////////////////////////////////////////////////
  // Procedure: Read the pins                   //
  ////////////////////////////////////////////////
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(enable1Pin, OUTPUT);
  
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
  pinMode(enable2Pin, OUTPUT);

  ledcSetup(pwmChannel, freq, resolution);
  ledcAttachPin(enable1Pin, pwmChannel);

  ledcSetup(pwmChannel, freq, resolution);
  ledcAttachPin(enable2Pin, pwmChannel);
  ////////////////////////////////////////////////

  Serial.begin(115200);
  
  ////////////////////////////////////////////////
  // Procedure: WiFi part                       //
  ////////////////////////////////////////////////
  // Set ESP32 as a Wi-Fi Station                 
  WiFi.mode(WIFI_STA);

  // Initilize ESP-NOW
  if (esp_now_init() != ESP_OK) {
    //Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Register callback function
  esp_now_register_recv_cb(OnDataRecv);
  ////////////////////////////////////////////////

  // delay(500);

  //SONAR Setup
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

}
 
void loop() 
{
  ////////////////////////////////////////////////
  // Procedure: Set the motors to               //
  //                rotate according to gyro    //
  ////////////////////////////////////////////////

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration_us = pulseIn(echoPin, HIGH);

  distance_cm = 0.017 * duration_us;

  Serial.print("distance: ");
  Serial.print(distance_cm);
  Serial.println(" cm");

  if (distance_cm > 10){
    dutyCycle = map((int)myData.AccelerationY, -10, 8, 50, 255);
    digitalWrite(motor1Pin1, HIGH);
    digitalWrite(motor1Pin2, LOW);
    digitalWrite(motor2Pin1, HIGH);
    digitalWrite(motor2Pin2, LOW);
    ledcWrite(pwmChannel, dutyCycle);
  }
  else if (distance_cm < 10){
    digitalWrite(motor1Pin1, LOW);
    digitalWrite(motor1Pin2, LOW);
    digitalWrite(motor2Pin1, LOW);
    digitalWrite(motor2Pin2, LOW);
  } 

  ////////////////////////////////////////////////
}
