/*
  Tutorial followed: https://aws.amazon.com/blogs/compute/building-an-aws-iot-core-device-using-aws-serverless-and-an-esp32/
  Instructions before burning the program to the board:
  1. Change the THING_NAME macros in "WifiEssentials/WifiEssntials.h" to the name you mentioned for your thing in AWS IoT Core
  2. Change the WIFI_SID in the same header file to your Wifi's name
  3. Change the WIFI_PASSWORd in the same header file to your Wifi's password
*/

#include "WifiEssentials/WifiEssentials.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"

// The MQTT topics that this device should publish/subscribe

#define     TURN_THRESHOLD_VAL    1.5

#define AWS_IOT_PUBLISH_TOPIC   "esp32/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC "esp32/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC_CAMERA  "esp32/cam"

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);

static int totalMessages = 0;

String Gesture = "";

typedef struct GyroscopeDataStruct {
  float AccelerationX;
  float AccelerationY;
  float AccelerationZ;
}
GyroscopeDataStruct;
GyroscopeDataStruct GyroscopeData;

////////////////////////////////////////////////
// Procedure:   MOTOR VARIABLES                //
////////////////////////////////////////////////

int motor1Pin1 = 27; 
int motor1Pin2 = 26; 
int enable1Pin = 14; 

int motor2Pin1 = 19;
int motor2Pin2 = 5; 
int enable2Pin = 18;

int leftIndicatorPin = 32;
int rightIndicatorPin = 22;
int stopIndicatorPin = 25;
bool hasStopped = false;
bool hasLeftIndicatorOn = false;
bool hasRightIndicatorOn = false;

const int freq = 30000;
const int pwmChannel = 0;
const int pwmChannel1 = 1;
const int resolution = 8;
int dutyCycle = 200;


int trigPin = 16;    // TRIG pin
int echoPin = 4;    // ECHO pin
float duration_us, distance_cm;

void setupMotorPins() {
    ////////////////////////////////////////////////
  // Procedure: Read the pins                   //
  ////////////////////////////////////////////////
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(enable1Pin, OUTPUT);
  
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
  pinMode(enable2Pin, OUTPUT);
  
  pinMode(leftIndicatorPin, OUTPUT);
  // pinMode(rightIndicatorPin, OUTPUT);
  pinMode(stopIndicatorPin, OUTPUT);

  ledcSetup(pwmChannel, freq, resolution);
  ledcAttachPin(enable1Pin, pwmChannel);

  ledcSetup(pwmChannel1, freq, resolution);
  ledcAttachPin(enable2Pin, pwmChannel1);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void writeMotorData() {
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration_us = pulseIn(echoPin, HIGH);

  distance_cm = 0.017 * duration_us;

  //Serial.print("distance: ");
  //Serial.print(distance_cm);
  //Serial.println(" cm");


  hasLeftIndicatorOn = false;
  hasRightIndicatorOn = false;
  if (distance_cm > 10){
    dutyCycle = map((int)GyroscopeData.AccelerationY, -10, 9, 50, 255);
    if(GyroscopeData.AccelerationX > TURN_THRESHOLD_VAL) {
        digitalWrite(motor1Pin1, LOW);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, HIGH);
        digitalWrite(motor2Pin2, LOW);
        //hasLeftIndicatorOn = true;
        digitalWrite(leftIndicatorPin, HIGH);
        //digitalWrite(leftIndicatorPin, HIGH);
    }
    else if(GyroscopeData.AccelerationX < -TURN_THRESHOLD_VAL) {
        digitalWrite(motor1Pin1, HIGH);
        digitalWrite(motor1Pin2, LOW);
        digitalWrite(motor2Pin1, LOW);
        digitalWrite(motor2Pin2, LOW);
        digitalWrite(rightIndicatorPin, HIGH);
        // hasRightIndicatorOn = true;
    }
    else {
      digitalWrite(motor1Pin1, HIGH);
      digitalWrite(motor1Pin2, LOW);
      digitalWrite(motor2Pin1, HIGH);
      digitalWrite(motor2Pin2, LOW);
      digitalWrite(leftIndicatorPin, LOW);
      digitalWrite(rightIndicatorPin, LOW);
    }
    Serial.println("Duty Cycle: " + String(dutyCycle));
    ledcWrite(pwmChannel, dutyCycle);
    ledcWrite(pwmChannel1, dutyCycle);
    hasStopped = false;
    if(dutyCycle <= 50) {
      hasStopped = true;
    }
    Serial.println(String(ledcRead(pwmChannel)) + "," + String(ledcRead(pwmChannel1)));
  }
  else if (distance_cm < 10){
    digitalWrite(motor1Pin1, LOW);
    digitalWrite(motor1Pin2, LOW);
    digitalWrite(motor2Pin1, LOW);
    digitalWrite(motor2Pin2, LOW);
    hasStopped = true;
  }
  // if(hasLeftIndicatorOn)
  //   digitalWrite(leftIndicatorPin, HIGH);
  // else
  //   digitalWrite(leftIndicatorPin, LOW);
  // if(hasRightIndicatorOn)
  //   digitalWrite(rightIndicatorPin, HIGH);
  // else
  //   digitalWrite(rightIndicatorPin, LOW);      
  if(hasStopped) {
    digitalWrite(stopIndicatorPin, HIGH);
  }
  else {
    digitalWrite(stopIndicatorPin, LOW);
  }
}

void connectAWS()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  // Configure WiFiClientSecure to use the AWS IoT device credentials
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);


  // Connect to the MQTT broker on the AWS endpoint we defined earlier
  client.begin(AWS_IOT_ENDPOINT, 8883, net);

  // Create a message handler
  client.onMessage(messageHandler);

  Serial.print("Connecting to AWS IOT");

  while (!client.connect(THING_NAME)) {
    Serial.print(".");
    delay(100);
  }
  Serial.println("");
  if(!client.connected()){
    Serial.println("AWS IoT Timeout!");
    return;
  }

  // Subscribe to a topic
  client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);
  client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC_CAMERA);

  Serial.println("AWS IoT Connected!");
}


void messageHandler(String &topic, String &payload) {
  Serial.println("Data received: " + topic + " - " + payload + " Total Messages: " + String(totalMessages));
  totalMessages++;
  StaticJsonDocument<200> doc;
  deserializeJson(doc, payload);
  String accelerationY = doc["aY"];
  String accelerationX = doc["aX"];
  GyroscopeData.AccelerationY = String(accelerationY).toFloat();
  Gesture = doc["Gesture"].as<String>();
  if(Gesture == "Left") {
    GyroscopeData.AccelerationX = 2;
  }
  else if(Gesture == "Right") {
    GyroscopeData.AccelerationX = -2;
  }
  else if(Gesture == "") GyroscopeData.AccelerationX = 0;
  //GyroscopeData.AccelerationX = String(accelerationX).toFloat();
  writeMotorData();
}

void setup() {
  Serial.begin(115200);
  connectAWS();
  setupMotorPins();
}

void loop() {
  client.loop();
  delay(125);
}

