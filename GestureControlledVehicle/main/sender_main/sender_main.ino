/**
  Tutorial followed: https://aws.amazon.com/blogs/compute/building-an-aws-iot-core-device-using-aws-serverless-and-an-esp32/
  Instructions before burning the program to the board:
  1. Change the THING_NAME macros in "WifiEssentials/WifiEssntials.h" to the name you mentioned for your thing in AWS IoT Core
  2. Change the WIFI_SID in the same header file to your Wifi's name
  3. Change the WIFI_PASSWORd in the same header file to your Wifi's password
**/

#include "WifiEssentials/WifiEssentials.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"

// Includes for gyroscope sensor
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>


// The MQTT topics that this device should publish/subscribe
#define AWS_IOT_PUBLISH_TOPIC   "esp32/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC "esp32/sub"

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);

Adafruit_MPU6050 mpu;


// Define a data structure
typedef struct GyroscopeDataStruct {
  float AccelerationX;
  float AccelerationY;
  float AccelerationZ;
  float RotationX;
  float RotationY;
  float RotationZ;
}
GyroscopeDataStruct;
GyroscopeDataStruct GyroscopeData;
 
sensors_event_t a, g, temp;

void connectAWS()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");

  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }



  // Configure WiFiClientSecure to use the AWS IoT device credentials
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);

  // Connect to the MQTT broker on the AWS endpoint we defined earlier
  client.begin(AWS_IOT_ENDPOINT, 8883, net);

  // Create a message handler
  client.onMessage(messageHandler);

  Serial.print("Connecting to AWS IOT");

  while (!client.connect(THING_NAME)) {
    Serial.print(".");
    delay(100);
  }

  if(!client.connected()){
    Serial.println("AWS IoT Timeout!");
    return;
  }

  // Subscribe to a topic
  //client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);

  Serial.println("AWS IoT Connected!");
}

void publishMessage()
{
  StaticJsonDocument<200> DataPacket;
  DataPacket["aX"] = GyroscopeData.AccelerationX;
  DataPacket["aY"] = GyroscopeData.AccelerationY;
  DataPacket["aZ"] = GyroscopeData.AccelerationZ;
  DataPacket["gX"] = GyroscopeData.RotationX;
  DataPacket["gY"] = GyroscopeData.RotationY;
  DataPacket["gZ"] = GyroscopeData.RotationZ;
  char jsonBuffer[512];
  serializeJson(DataPacket, jsonBuffer); // print to client

  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
  Serial.println("Publishing data: " + String(jsonBuffer));
  delay(500);
}

void messageHandler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
}

void setup() {
  Serial.begin(9600);
  SetupMPU6050();
  connectAWS();
}

void loop() {
  GetGyroscopeData();
  publishMessage();
  client.loop();
  delay(100);
}
void SetupMPU6050(){
  Serial.println("**************************************************");
  Serial.println("Setting up the MPU-6050 module");
  // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");

  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Serial.print("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
  case MPU6050_RANGE_2_G:
    Serial.println("+-2G");
    break;
  case MPU6050_RANGE_4_G:
    Serial.println("+-4G");
    break;
  case MPU6050_RANGE_8_G:
    Serial.println("+-8G");
    break;
  case MPU6050_RANGE_16_G:
    Serial.println("+-16G");
    break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Serial.print("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
  case MPU6050_RANGE_250_DEG:
    Serial.println("+- 250 deg/s");
    break;
  case MPU6050_RANGE_500_DEG:
    Serial.println("+- 500 deg/s");
    break;
  case MPU6050_RANGE_1000_DEG:
    Serial.println("+- 1000 deg/s");
    break;
  case MPU6050_RANGE_2000_DEG:
    Serial.println("+- 2000 deg/s");
    break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_5_HZ);
  Serial.print("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
  case MPU6050_BAND_260_HZ:
    Serial.println("260 Hz");
    break;
  case MPU6050_BAND_184_HZ:
    Serial.println("184 Hz");
    break;
  case MPU6050_BAND_94_HZ:
    Serial.println("94 Hz");
    break;
  case MPU6050_BAND_44_HZ:
    Serial.println("44 Hz");
    break;
  case MPU6050_BAND_21_HZ:
    Serial.println("21 Hz");
    break;
  case MPU6050_BAND_10_HZ:
    Serial.println("10 Hz");
    break;
  case MPU6050_BAND_5_HZ:
    Serial.println("5 Hz");
    break;
  }
  Serial.println("Done setting up MPU-6050 module..");
  Serial.println("**************************************************");
}
void GetGyroscopeData()
{
  mpu.getEvent(&a, &g, &temp);
  GyroscopeData.AccelerationX = a.acceleration.x;
  GyroscopeData.AccelerationY = a.acceleration.y;
  GyroscopeData.AccelerationZ = a.acceleration.z;
  GyroscopeData.RotationX = g.gyro.x;
  GyroscopeData.RotationY = g.gyro.y;
  GyroscopeData.RotationZ = g.gyro.z;
}