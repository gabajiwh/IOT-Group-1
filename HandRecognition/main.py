# import necessary packages
"""
    AWS CORE : CODE START
"""

import AWSIoTPythonSDK.MQTTLib as AWSIoTPyMQTT
import cv2

import mediapipe as mp
import tensorflow as tf
from tensorflow.keras.models import load_model

import time as t
import json
import numpy as np

import threading

ENDPOINT = ""
CLIENT_ID = "ESP32_CAM_DEVICE"
PATH_TO_CERTIFICATE = ""
PATH_TO_PRIVATE_KEY = ""
PATH_TO_AMAZON_ROOT_CA_1 = "data/AmazonRootCA1 (1).pem"
MESSAGE = "Hello World"
TOPIC = "esp32/cam"
RANGE = 20

"""
    AWS CORE : CODE END
"""

# initialize mediapipe
mpHands = mp.solutions.hands
hands = mpHands.Hands(max_num_hands=1, min_detection_confidence=0.7)
mpDraw = mp.solutions.drawing_utils

# Load the gesture recognizer model
model = load_model('mp_hand_gesture')

# Load class names
f = open('gesture.names', 'r')
classNames = f.read().split('\n')
f.close()
print(classNames)


"""
    AWS CORE : Initializing AWS
"""
myAWSIoTMQTTClient = AWSIoTPyMQTT.AWSIoTMQTTClient(CLIENT_ID)
myAWSIoTMQTTClient.configureEndpoint(ENDPOINT, 8883)
myAWSIoTMQTTClient.configureCredentials(PATH_TO_AMAZON_ROOT_CA_1, PATH_TO_PRIVATE_KEY, PATH_TO_CERTIFICATE)
myAWSIoTMQTTClient.connect()
"""
    AWS CORE: End Initializing
"""
# Initialize the webcam
#cap = cv2.VideoCapture(0)
url = "http://192.168.154.253/mjpeg/1"
cap = cv2.VideoCapture()
cap.open(url)


gesture_list = []

current_gesture = ""
last_gesture = ""


while True:
    # Read each frame from the webcam
    #_, frame = cap.read()
    ret, frame = cap.read()
    x, y, c = frame.shape
    

    # Flip the frame vertically
    frame = cv2.flip(frame, 1)
    framergb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # Get hand landmark prediction
    result = hands.process(framergb)

    # print(result)
    
    className = ''
    """
        [ '1','1','1','1','1','1','1'] 2
        [ '1','1','1','1','1','1','2'] 1
    """
    # post process the result
    if result.multi_hand_landmarks:
        landmarks = []
        for handslms in result.multi_hand_landmarks:
            for lm in handslms.landmark:
                # print(id, lm)
                lmx = int(lm.x * x)
                lmy = int(lm.y * y)

                landmarks.append([lmx, lmy])

            # Drawing landmarks on frames
            #mpDraw.draw_landmarks(frame, handslms, mpHands.HAND_CONNECTIONS)

            # Predict gesture
            prediction = model.predict([landmarks])
            # print(prediction)
            classID = np.argmax(prediction)
            className = classNames[classID]
    
    # current_gesture = className
    # if len(gesture_list) < 10:
    #     gesture_list.append(className)
    # else:
    #     gesture_list.pop(0)
    #     gesture_list.append(className)

    # count_dict = {}
    # for i in gesture_list:
    #     count_dict[i] = gesture_list.count(i)
    # #print(gesture_list)
    # #print(count_dict)
    # best_gesture = max(count_dict, key=count_dict.get)
    # #print(best_gesture)
    # if best_gesture != last_gesture:
    #     message = {"Gesture" : best_gesture}
    #     #print(message)
    #     myAWSIoTMQTTClient.publish(TOPIC, json.dumps(message), 1) 
    #     #   print("Published: '" + json.dumps(message) + "' to the topic: " + "'test/testing'")
    #     #t.sleep(0.1)
    #     last_gesture = best_gesture
    message = {"Gesture" : className}
        #print(message)
    myAWSIoTMQTTClient.publish(TOPIC, json.dumps(message), 1) 
    # show the prediction on the frame
    cv2.putText(frame, className, (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA)

    # Show the final output
    cv2.imshow("Laptop - Video Feed", frame) 

    if cv2.waitKey(1) == ord('q'):
        break

# release the webcam and destroy all active windows
cap.release()
cv2.destroyAllWindows()
myAWSIoTMQTTClient.disconnect()
