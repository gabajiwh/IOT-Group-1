# Gesture Controlled Vehicle

## About

The Gesture Controlled Vehicle (GSV) is an Internet of Things (IoT) solution designed for remote controlled vehicles. It employs various components, including the ESP32 microcontroller, ESP-EYE, L293D motor driver, HC-SR04 ultrasonic sensor, and MPU-6050 Gyroscope. The project's primary objective is to create a functionality to detect gesture of a person from the image captured by ESP-EYE and to use the MQTT protocol to establish global communication between IoT devices through cloud. It was created as a deliverable for the Internet of Things (CS7NS2) module.

## Proposed Solution

The proposed solution provided by the GSV addresses the limitations of traditional remote controlled vehicles that are typically controlled through button inputs. By incorporating the MPU-6050 gyroscope module, the GSV can detect the orientation of the ESP32 controller and use it to control the vehicle. Additionally, the ESP-EYE module with a gesture recognition algorithm has been utilized to provide more advanced controls over the vehicle.

## Project Architecture
![Project Architecture](.images/arch.png)

## Hardware Requirements

| Name                              | No. of units         | Description                     |
|-----------------------------------|----------------------|---------------------------------| 
| DC Motors (with wheels)           | 2 / 4                |  For motor functionality        |
| L293D motor driver IC             | 1                    |  For motor functionality (providing better motor control) |
| ESP32 microcontroller             | 2                    |  The brain for the control and vehicle |
| ESP-EYE                           | 1                    |  The gesture recognition device for turning the vheicle and additional vehicle controls|
| MPU-6050                          | 1                    |  Gyroscope module for driving the vehicle|
| HC-SR04                           | 1                    |  SONAR feedback module for object detection |


## Software used

**Arduino IDE** (https://www.arduino.cc/en/software)

**Version used**: 2.0.4

## Library Dependencies


| Library Name                              | Author         | Version                     |
|-----------------------------------|----------------------|---------------------------------| 
| **ESP32** | _Espressif Systems_ | 2.0.7 |
| **Adafruit MPU6050** | _Adafruit_ | 2.2.3 |
| **Arduino JSON** | _Benoit Blanchon_ | 6.21.1 |
| **MQTT** | _Joel Gaehwiler_ | 2.5.0 |

## Code Structure

The main code for the Gesture Controlled Vehicle is located in the directory *GestureControlledVehicle\main* and includes the implementation of the **Sender**, **Receiver**, and **ESP-EYE** camera. The folder *esp-eye* contains the code for the ESP-EYE camera, *receiver_main* contains the code for the *ESP32* receiver that will be positioned on top of the vehicle, and *sender_main* contains the code for the ESP32 sender that will transmit motion signals for the vehicle using a gyroscope.

## Team Members

1. Abhik Subir Bhattacharjee (`bhattaa2@tcd.ie`)
2. Hamza Gabajiwala (`gabajiwh@tcd.ie`)
3. Milan Mukherjee (`mukherjm@tcd.ie`)
4. Rohit Das (`dasro@tcd.ie`)
5. Siddharth Satish Shenoy (`shenoys@tcd.ie`)

## References

[1]  _Team, T.V. (2023) Real-time hand gesture recognition using tensorflow &amp; opencv, TechVidvan. Available at: https://techvidvan.com/tutorials/hand-gesture-recognition-tensorflow-opencv/ (Accessed: March 29, 2023)._

